#Drone Delivery System API
This web application handles all API request for Drone Delivery Syste.

##Pre requists
- Java 11
Install Oracle JDK 1.11 using this [link](https://www.oracle.com/java/technologies/downloads/#java11)
- Maven
Install apache maven using this [link](https://maven.apache.org/install.html)

##Used Technologies
- Spring Boot
- H2 database (in memmory)
- Swagger

##Usage
###Build and Run
- Clone the project to local
- Go to project root directory
- Run `mvn install` to install dependencies
- Run `mvn package` to build the jar file.
- Go to target forlder and run the `java -jar drone-delivery-0.0.1-SNAPSHOT.jar` to run the application.

###API Documentation
- [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

###Database Console
- [http://localhost:8080/h2-console](http://localhost:8080/h2-console "http://localhost:8080/h2-console")

package com.rumaiz.dronedelivery.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.rumaiz.dronedelivery.converters.StringToDroneModelEnumConverter;
import com.rumaiz.dronedelivery.converters.StringToDroneStateConverter;

@Configuration
@EnableScheduling
public class WebConfig implements WebMvcConfigurer{
	
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new StringToDroneModelEnumConverter());
		registry.addConverter(new StringToDroneStateConverter());
	}
}

package com.rumaiz.dronedelivery.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rumaiz.dronedelivery.dtos.CreateDeliveryOrderDto;
import com.rumaiz.dronedelivery.dtos.LoadMedicationDto;
import com.rumaiz.dronedelivery.dtos.LoadedMedicationItemDto;
import com.rumaiz.dronedelivery.model.DeliveryOrder;
import com.rumaiz.dronedelivery.model.DeliveryOrderDetailes;
import com.rumaiz.dronedelivery.model.Drone;
import com.rumaiz.dronedelivery.service.DroneService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/drone")
public class DroneController {
	
	private DroneService droneService;
	
	@Autowired
	public void setDroneService(DroneService droneService) {
		this.droneService = droneService;
	}

	@ApiOperation(value = "Regiter the new drone")
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public void insert(@RequestBody Drone drone) {
		this.droneService.insert(drone);
	}
	
	@ApiOperation(value = "Get all drones")
	@GetMapping
	@ResponseBody
	public List<Drone> getAll(){
		return this.droneService.getAll();
	}
	
	@ApiOperation(value = "Create delivery order and allocate a drone")
	@PostMapping("/order")
	@ResponseBody
	public DeliveryOrder createOrder(@RequestBody CreateDeliveryOrderDto dto) {
		return this.droneService.createOrder(dto);
	}
	
	@ApiOperation(value = "Load medication to drone")
	@PutMapping("/order/medication")
	@ResponseBody
	public DeliveryOrderDetailes loadMedication(@RequestBody LoadMedicationDto dto){
		return this.droneService.loadMedication(dto);
	}
	
	@ApiOperation(value = "Get medications loaded to the drone")
	@GetMapping("/{droneSerialNumber}/order/medication")
	@ResponseBody
	public List<LoadedMedicationItemDto> getLoadedMedication(@PathVariable("droneSerialNumber") String droneSerialNumber){
		return this.droneService.getLoadedMedication(droneSerialNumber);
	}
	
	@ApiOperation(value = "Get available drones for loading")
	@GetMapping("/available")
	public List<Drone> getAvailableDrones(){
		return this.droneService.getAvailableDrones();
	}
	
	@ApiOperation(value = "Complete loading for order")
	@PutMapping("/order/{orderId}/complete-loading")
	@ResponseBody
	public void completeLoading(@PathVariable("orderId") int orderId){
		this.droneService.completeLoading(orderId);
	}
	
	@ApiOperation(value = "Dispatch order")
	@PutMapping("/order/{orderId}/dispatch")
	@ResponseBody
	public void dispatchOrder(@PathVariable("orderId") int orderId){
		this.droneService.dispatchOrder(orderId);
	}
	
	@ApiOperation(value = "Update order delivered")
	@PutMapping("/order/{orderId}/delivered")
	@ResponseBody
	public Drone orderDelivered(@PathVariable("orderId") int orderId){
		return this.droneService.setOrderDelivered(orderId);
	}
	
	@ApiOperation(value = "Dispatch order")
	@PutMapping("/order/{orderId}/delivery-failed")
	@ResponseBody
	public Drone orderDeliveryFailed(@PathVariable("orderId") int orderId){
		return this.droneService.setOrderDeliveryFailed(orderId);
	}
	
	@ApiOperation(value = "Update drone returned to fleet")
	@PutMapping("/{serialNumber}/returned")
	@ResponseBody
	public Drone orderDeliveryFailed(@PathVariable("serialNumber") String serialNumber){
		return this.droneService.droneReturnedToFleet(serialNumber);
	}
}

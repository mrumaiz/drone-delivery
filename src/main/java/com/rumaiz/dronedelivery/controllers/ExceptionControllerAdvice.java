package com.rumaiz.dronedelivery.controllers;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.rumaiz.dronedelivery.exceptions.DroneDeliveryException;


@ControllerAdvice
public class ExceptionControllerAdvice {
	
	@ExceptionHandler(DroneDeliveryException.class)
	public ResponseEntity<Object> handleDroneDeliveryExceptions(DroneDeliveryException ex, WebRequest request){
		Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("error", ex.getHttpStatus().getReasonPhrase());
        body.put("message", ex.getMessage());
        body.put("status", ex.getHttpStatus().value());
        return new ResponseEntity<>(body, ex.getHttpStatus());
	}
}

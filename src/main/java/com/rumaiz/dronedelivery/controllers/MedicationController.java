package com.rumaiz.dronedelivery.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rumaiz.dronedelivery.model.Medication;
import com.rumaiz.dronedelivery.service.MedicationService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/medication")
public class MedicationController {
	
	private MedicationService medicationService;

	@Autowired
	public void setMedicationService(MedicationService medicationService) {
		this.medicationService = medicationService;
	}
	
	@ApiOperation(value = "Add new Medication")
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public void insert(@RequestBody Medication medication) {
		this.medicationService.insert(medication);
	}
	
	@ApiOperation(value = "Get all medications")
	@GetMapping
	@ResponseBody
	public List<Medication> getAll(){
		return this.medicationService.getAll();
	}
	
}

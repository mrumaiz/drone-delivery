package com.rumaiz.dronedelivery.converters;

import org.springframework.core.convert.converter.Converter;

import com.rumaiz.dronedelivery.enums.DroneModelEnum;

public class StringToDroneModelEnumConverter implements Converter<String, DroneModelEnum>{

	@Override
	public DroneModelEnum convert(String source) {
		return DroneModelEnum.valueOf(source);
	}
	
}

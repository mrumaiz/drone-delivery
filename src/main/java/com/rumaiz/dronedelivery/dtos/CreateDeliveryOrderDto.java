package com.rumaiz.dronedelivery.dtos;

public class CreateDeliveryOrderDto {
	
	private String droneSerialNumber;
	
	private String location;

	public String getDroneSerialNumber() {
		return droneSerialNumber;
	}

	public void setDroneSerialNumber(String droneSerialNumber) {
		this.droneSerialNumber = droneSerialNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}

package com.rumaiz.dronedelivery.dtos;

public class LoadMedicationDto {
	private int orderId;
	private String medicationCode;
	private int quatity;
	
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getMedicationCode() {
		return medicationCode;
	}
	public void setMedicationCode(String medicationCode) {
		this.medicationCode = medicationCode;
	}
	public int getQuatity() {
		return quatity;
	}
	public void setQuatity(int quatity) {
		this.quatity = quatity;
	}
	
}

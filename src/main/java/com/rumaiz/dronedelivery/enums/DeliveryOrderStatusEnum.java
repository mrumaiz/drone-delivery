package com.rumaiz.dronedelivery.enums;

public enum DeliveryOrderStatusEnum {
	LOADING,
	LOADED,
	DEPARTED,
	DELIVERED,
	DELIVERY_FAILED
}

package com.rumaiz.dronedelivery.enums;

public enum DroneStateEnum {
	IDLE,
	LOADING,
	LOADED,
	DELIVERING,
	DELIVERED,
	RETURNING
}

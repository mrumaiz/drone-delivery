package com.rumaiz.dronedelivery.exceptions;

import org.springframework.http.HttpStatus;

public class DroneDeliveryException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private HttpStatus httpStatus;
	
	public DroneDeliveryException(String message) {
		super(message);
		this.httpStatus = HttpStatus.OK;
	}
	
	public DroneDeliveryException(String message, HttpStatus status) {
		super(message);
		this.httpStatus = status;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
}

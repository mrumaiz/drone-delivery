package com.rumaiz.dronedelivery.exceptions;

import org.springframework.http.HttpStatus;

public class InvalidInputDataException extends DroneDeliveryException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidInputDataException(String message) {
		super(message, HttpStatus.BAD_REQUEST);
	}

}

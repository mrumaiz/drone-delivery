package com.rumaiz.dronedelivery.exceptions;

import org.springframework.http.HttpStatus;

public class ObjectNotFoundException extends DroneDeliveryException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ObjectNotFoundException(String message) {
		super(message, HttpStatus.NOT_FOUND);
	}
}

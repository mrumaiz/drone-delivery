package com.rumaiz.dronedelivery.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.rumaiz.dronedelivery.enums.DeliveryOrderStatusEnum;

@Entity
@Table(name = "delivery_order")
public class DeliveryOrder {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Drone drone;
	
	private String location;
	
	@Column(name = "created_on")
	private Date createdOn;
	
	@Column(name = "completed_on", nullable = true)
	private Date completedOn;
	
	@Enumerated(EnumType.STRING)
	private DeliveryOrderStatusEnum status;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "deliveryOrder")
	private List<DeliveryOrderDetailes> orderDetails = new ArrayList<DeliveryOrderDetailes>();
	
	@Column(name = "loaded_weight")
	private int loadedWeight;
	
	@Column(name = "is_active")
	private int isActive;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Drone getDrone() {
		return drone;
	}

	public void setDrone(Drone drone) {
		this.drone = drone;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getCompletedOn() {
		return completedOn;
	}

	public void setCompletedOn(Date completedOn) {
		this.completedOn = completedOn;
	}

	public DeliveryOrderStatusEnum getStatus() {
		return status;
	}

	public void setStatus(DeliveryOrderStatusEnum status) {
		this.status = status;
	}

	public List<DeliveryOrderDetailes> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(List<DeliveryOrderDetailes> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public int getLoadedWeight() {
		return loadedWeight;
	}

	public void setLoadedWeight(int loadedWeight) {
		this.loadedWeight = loadedWeight;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	
}

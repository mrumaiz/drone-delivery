package com.rumaiz.dronedelivery.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.rumaiz.dronedelivery.enums.DroneModelEnum;
import com.rumaiz.dronedelivery.enums.DroneStateEnum;

/**
 * This class represent the drone object which saved in the db.
 * @author Mohomed Rumaiz
 * @since 18-12-2021
 */

@Entity
public class Drone {
	@Id
	@Column(name = "serial_number")
	private String serialNumber;
	
	@Enumerated(EnumType.STRING)
	private DroneModelEnum model;
	
	@Column(name = "weight_limit")
	private int weightLimit;
	
	@Column(name = "battery_capacity")
	private int batteryCapacity;
	
	@Enumerated(EnumType.STRING)
	private DroneStateEnum state;
	
	public String getSerialNumber() {
		return serialNumber;
	}
	
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	public DroneModelEnum getModel() {
		return model;
	}
	
	public void setModel(DroneModelEnum model) {
		this.model = model;
	}
	
	public int getWeightLimit() {
		return weightLimit;
	}
	
	public void setWeightLimit(int weightLimit) {
		this.weightLimit = weightLimit;
	}
	
	public int getBatteryCapacity() {
		return batteryCapacity;
	}
	
	public void setBatteryCapacity(int batteryCapacity) {
		this.batteryCapacity = batteryCapacity;
	}
	
	public DroneStateEnum getState() {
		return state;
	}
	
	public void setState(DroneStateEnum state) {
		this.state = state;
	}
}

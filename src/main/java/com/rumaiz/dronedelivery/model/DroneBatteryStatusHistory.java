package com.rumaiz.dronedelivery.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "drone_battery_capacity_history")
public class DroneBatteryStatusHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	
	@Column(name = "checked_on")
	private Date checkedOn;
	
	@Column(name = "previous_reading")
	private int previousReading;
	
	@Column(name = "new_reading")
	private int newReading;
	
	@ManyToOne
	private Drone drone;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCheckedOn() {
		return checkedOn;
	}

	public void setCheckedOn(Date checkedOn) {
		this.checkedOn = checkedOn;
	}

	public int getPreviousReading() {
		return previousReading;
	}

	public void setPreviousReading(int previousReading) {
		this.previousReading = previousReading;
	}

	public int getNewReading() {
		return newReading;
	}

	public void setNewReading(int newReading) {
		this.newReading = newReading;
	}

	public Drone getDrone() {
		return drone;
	}

	public void setDrone(Drone drone) {
		this.drone = drone;
	}
	
}

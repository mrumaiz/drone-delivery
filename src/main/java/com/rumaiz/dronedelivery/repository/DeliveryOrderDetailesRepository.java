package com.rumaiz.dronedelivery.repository;

import org.springframework.data.repository.CrudRepository;

import com.rumaiz.dronedelivery.model.DeliveryOrderDetailes;

public interface DeliveryOrderDetailesRepository extends CrudRepository<DeliveryOrderDetailes, String>{ 

}

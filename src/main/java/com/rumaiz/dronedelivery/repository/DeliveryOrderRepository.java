package com.rumaiz.dronedelivery.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.rumaiz.dronedelivery.model.DeliveryOrder;

public interface DeliveryOrderRepository extends CrudRepository<DeliveryOrder, Integer>{
	
	@Query(value = "SELECT * FROM delivery_order WHERE drone_serial_number=?1 AND is_active=1", nativeQuery = true)
	public DeliveryOrder findDeliveryOrderByStatusAndDrone(String droneSerialNumber);
}

package com.rumaiz.dronedelivery.repository;

import org.springframework.data.repository.CrudRepository;

import com.rumaiz.dronedelivery.model.DroneBatteryStatusHistory;

public interface DroneBatteryStatusHistoryRepository extends CrudRepository<DroneBatteryStatusHistory, Integer>{

}

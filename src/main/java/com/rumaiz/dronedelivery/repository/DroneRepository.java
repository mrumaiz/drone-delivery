package com.rumaiz.dronedelivery.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.rumaiz.dronedelivery.enums.DroneStateEnum;
import com.rumaiz.dronedelivery.model.Drone;

public interface DroneRepository extends CrudRepository<Drone, String>{
	
	@Query("SELECT d FROM Drone d WHERE d.state=?1")
	public List<Drone> findAllByDroneStatus(DroneStateEnum droneState);
	
	@Query("SELECT d FROM Drone d WHERE d.state=?1 AND d.batteryCapacity >= 25")
	public List<Drone> findAllAllocatabledrones(DroneStateEnum droneState);
}

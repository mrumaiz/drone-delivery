package com.rumaiz.dronedelivery.repository;

import org.springframework.data.repository.CrudRepository;

import com.rumaiz.dronedelivery.model.Medication;

public interface MedicationRepository extends CrudRepository<Medication, String>{

}

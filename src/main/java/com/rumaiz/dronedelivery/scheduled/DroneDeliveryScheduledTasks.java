package com.rumaiz.dronedelivery.scheduled;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.rumaiz.dronedelivery.model.Drone;
import com.rumaiz.dronedelivery.service.DroneService;

public class DroneDeliveryScheduledTasks {
	private DroneService droneService;

	@Autowired
	public void setDroneService(DroneService droneService) {
		this.droneService = droneService;
	}
	
	@Scheduled(fixedDelay = 60000)
	public void updateDroneBatteryStatus() {
		List<Drone> drones = this.droneService.getAll();
		for (Drone drone : drones) {
			int newBatteryCapacity = this.getNewBatteryCapacity(drone);
			this.droneService.updateDroneBatteryCapacity(drone, newBatteryCapacity);
		}
	}
	
	/**
	 * This method will return random number as new battery capacity
	 */
	private int getNewBatteryCapacity(Drone drone) {
		Random r = new Random();
		int low = 0;
		int high = 100;
		int result = r.nextInt(high-low) + low;
		return result;
	}
}

package com.rumaiz.dronedelivery.service;

import java.util.List;

import com.rumaiz.dronedelivery.dtos.CreateDeliveryOrderDto;
import com.rumaiz.dronedelivery.dtos.LoadMedicationDto;
import com.rumaiz.dronedelivery.dtos.LoadedMedicationItemDto;
import com.rumaiz.dronedelivery.model.DeliveryOrder;
import com.rumaiz.dronedelivery.model.DeliveryOrderDetailes;
import com.rumaiz.dronedelivery.model.Drone;

public interface DroneService {
	public void insert(Drone drone);
	public List<Drone> getAll();
	public DeliveryOrder createOrder(CreateDeliveryOrderDto dto);
	public DeliveryOrderDetailes loadMedication(LoadMedicationDto dto);
	public List<LoadedMedicationItemDto> getLoadedMedication(String droneSerialNumber);
	public List<Drone> getAvailableDrones();
	public void completeLoading(int orderId);
	public void dispatchOrder(int orderId);
	public Drone setOrderDelivered(int orderId);
	public Drone setOrderDeliveryFailed(int orderId);
	public Drone droneReturnedToFleet(String droneSerialNumber);
	public Drone updateDroneBatteryCapacity(Drone drone, int batteryCapacity);
}

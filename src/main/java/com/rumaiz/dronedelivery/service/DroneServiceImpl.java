package com.rumaiz.dronedelivery.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rumaiz.dronedelivery.dtos.CreateDeliveryOrderDto;
import com.rumaiz.dronedelivery.dtos.LoadMedicationDto;
import com.rumaiz.dronedelivery.dtos.LoadedMedicationItemDto;
import com.rumaiz.dronedelivery.enums.DeliveryOrderStatusEnum;
import com.rumaiz.dronedelivery.enums.DroneStateEnum;
import com.rumaiz.dronedelivery.exceptions.DroneDeliveryException;
import com.rumaiz.dronedelivery.exceptions.InvalidInputDataException;
import com.rumaiz.dronedelivery.exceptions.ObjectNotFoundException;
import com.rumaiz.dronedelivery.model.DeliveryOrder;
import com.rumaiz.dronedelivery.model.DeliveryOrderDetailes;
import com.rumaiz.dronedelivery.model.Drone;
import com.rumaiz.dronedelivery.model.DroneBatteryStatusHistory;
import com.rumaiz.dronedelivery.model.Medication;
import com.rumaiz.dronedelivery.repository.DeliveryOrderDetailesRepository;
import com.rumaiz.dronedelivery.repository.DeliveryOrderRepository;
import com.rumaiz.dronedelivery.repository.DroneBatteryStatusHistoryRepository;
import com.rumaiz.dronedelivery.repository.DroneRepository;

@Service
public class DroneServiceImpl implements DroneService{
	
	private DroneRepository droneRepository;
	
	private DeliveryOrderRepository deliveryOrderRepository;
	
	private MedicationService medicationService;
	
	private DeliveryOrderDetailesRepository orderDetailesRepository;
	
	private DroneBatteryStatusHistoryRepository droneBatteryStatusHistoryRepository;

	@Autowired
	public void setDroneRepository(DroneRepository droneRepository) {
		this.droneRepository = droneRepository;
	}

	@Autowired
	public void setDeliveryOrderRepository(DeliveryOrderRepository deliveryOrderRepository) {
		this.deliveryOrderRepository = deliveryOrderRepository;
	}
	
	@Autowired
	public void setMedicationService(MedicationService medicationService) {
		this.medicationService = medicationService;
	}
	
	@Autowired
	public void setOrderDetailesRepository(DeliveryOrderDetailesRepository orderDetailesRepository) {
		this.orderDetailesRepository = orderDetailesRepository;
	}
	
	@Autowired
	public void setDroneBatteryStatusHistoryRepository(
			DroneBatteryStatusHistoryRepository droneBatteryStatusHistoryRepository) {
		this.droneBatteryStatusHistoryRepository = droneBatteryStatusHistoryRepository;
	}

	@Override
	public void insert(Drone drone) {
		if (drone.getSerialNumber().length() > 100) {
			throw new InvalidInputDataException("Serial Number should be maximum 100 characters");
		}
		
		if (drone.getWeightLimit() > 500) {
			throw new InvalidInputDataException("Weight Limit should be less than or equal to 500 grams");
		}
		
		if (drone.getBatteryCapacity() > 100 || drone.getBatteryCapacity() < 0) {
			throw new InvalidInputDataException("Invalid Battery Capacity.");
		}
		
		this.droneRepository.save(drone);
	}

	@Override
	public List<Drone> getAll() {
		return (List<Drone>) this.droneRepository.findAll();
	}

	@Override
	@Transactional
	public DeliveryOrder createOrder(CreateDeliveryOrderDto dto) {
		Optional<Drone> droneOptional = this.droneRepository.findById(dto.getDroneSerialNumber());
		if (!droneOptional.isPresent()) {
			throw new ObjectNotFoundException("Drone not found for serial number: " + dto.getDroneSerialNumber());
		}
		
		Drone drone = droneOptional.get();
		if (!drone.getState().equals(DroneStateEnum.IDLE)) {
			throw new DroneDeliveryException("Drone is not in IDLE to start loading.");
		}
		
		if (drone.getBatteryCapacity() < 25) {
			throw new DroneDeliveryException("Drone battery capacity islower that required capacity.");
		}
		drone.setState(DroneStateEnum.LOADING);
		this.droneRepository.save(drone);
		
		DeliveryOrder order = new DeliveryOrder();
		order.setCreatedOn(new Date());
		order.setDrone(drone);
		order.setLocation(dto.getLocation());
		order.setStatus(DeliveryOrderStatusEnum.LOADING);
		order.setIsActive(1);
		this.deliveryOrderRepository.save(order);
		
		return order;
	}

	@Override
	@Transactional
	public DeliveryOrderDetailes loadMedication(LoadMedicationDto dto) {
		Optional<DeliveryOrder> orderOptional = this.deliveryOrderRepository.findById(dto.getOrderId());
		if (orderOptional.isEmpty()) {
			throw new ObjectNotFoundException("Delivery order not found.");
		}
		
		DeliveryOrder order = orderOptional.get();
		if (order.getIsActive() == 0) {
			throw new DroneDeliveryException("Delivery order is not active.");
		}
		Medication medication = this.medicationService.getByCode(dto.getMedicationCode());
		
		Drone drone = order.getDrone();
		if (!drone.getState().equals(DroneStateEnum.LOADING)) {
			throw new DroneDeliveryException("Drone is not in LOADING mode.");
		}
		
		int loadingWeight = medication.getWeight() * dto.getQuatity();
		int newLoadedWeight = order.getLoadedWeight() + loadingWeight;
		if (newLoadedWeight > drone.getWeightLimit()) {
			throw new DroneDeliveryException("Drone weight limit exceeding. Cannot load.");
		}
		order.setLoadedWeight(newLoadedWeight);
		
		DeliveryOrderDetailes orderDetailes = new DeliveryOrderDetailes();
		orderDetailes.setMedication(medication);
		orderDetailes.setQuantity(newLoadedWeight);
		orderDetailes.setQuantity(dto.getQuatity());
		orderDetailes.setWeight(loadingWeight);
		orderDetailes.setDeliveryOrder(order);
		this.orderDetailesRepository.save(orderDetailes);
		
		order.getOrderDetails().add(orderDetailes);
		this.deliveryOrderRepository.save(order);
		
		return orderDetailes;
	}

	@Override
	public List<LoadedMedicationItemDto> getLoadedMedication(String droneSerialNumber) {
		Optional<Drone> droneOptional = this.droneRepository.findById(droneSerialNumber);
		if (!droneOptional.isPresent()) {
			throw new ObjectNotFoundException("Drone not found.");
		}
		
		Drone drone = droneOptional.get();
		if (drone.getState().equals(DroneStateEnum.DELIVERED) || drone.getState().equals(DroneStateEnum.RETURNING) 
				|| drone.getState().equals(DroneStateEnum.IDLE)) {
			return new ArrayList<>();
		}
		DeliveryOrder order = this.deliveryOrderRepository.findDeliveryOrderByStatusAndDrone(droneSerialNumber);
		if (order == null) {
			return new ArrayList<>();
		}
		
		List<LoadedMedicationItemDto> returnList = new ArrayList<LoadedMedicationItemDto>();
		for (DeliveryOrderDetailes orderDetailes : order.getOrderDetails()) {
			LoadedMedicationItemDto dto = new LoadedMedicationItemDto();
			dto.setCode(orderDetailes.getMedication().getCode());
			dto.setImagePath(orderDetailes.getMedication().getImagePath());
			dto.setQuantity(orderDetailes.getQuantity());
			dto.setName(orderDetailes.getMedication().getName());
			dto.setWeight(orderDetailes.getMedication().getWeight() * orderDetailes.getQuantity());
			returnList.add(dto);
		}
		
		return returnList;
	}

	@Override
	public List<Drone> getAvailableDrones() {
		return this.droneRepository.findAllAllocatabledrones(DroneStateEnum.IDLE);
	}
	
	private DeliveryOrder getActiveOrder(int orderId) {
		Optional<DeliveryOrder> orderOptional = this.deliveryOrderRepository.findById(orderId);
		if (orderOptional.isEmpty()) {
			throw new ObjectNotFoundException("Delivery order not found.");
		}
		
		DeliveryOrder order = orderOptional.get();
		if (order.getIsActive() == 0) {
			throw new DroneDeliveryException("Delivery order is not active.");
		}
		return order;
	}

	@Override
	@Transactional
	public void completeLoading(int orderId) {
		DeliveryOrder order = this.getActiveOrder(orderId);
		
		Drone drone = order.getDrone();
		if (!drone.getState().equals(DroneStateEnum.LOADING)) {
			throw new DroneDeliveryException("Drone is not in LOADING mode.");
		}
		
		drone.setState(DroneStateEnum.LOADED);
		this.droneRepository.save(drone);
		
		order.setStatus(DeliveryOrderStatusEnum.LOADED);
		this.deliveryOrderRepository.save(order);
	}

	@Override
	@Transactional
	public void dispatchOrder(int orderId) {
		DeliveryOrder order = this.getActiveOrder(orderId);
		
		Drone drone = order.getDrone();
		if (!drone.getState().equals(DroneStateEnum.LOADED)) {
			throw new DroneDeliveryException("Drone is not in LOADED mode.");
		}
		
		drone.setState(DroneStateEnum.DELIVERING);
		this.droneRepository.save(drone);
		
		order.setStatus(DeliveryOrderStatusEnum.DEPARTED);
		this.deliveryOrderRepository.save(order);
	}

	@Override
	@Transactional
	public Drone setOrderDelivered(int orderId) {
		DeliveryOrder order = this.getActiveOrder(orderId);
		
		Drone drone = order.getDrone();
		if (!drone.getState().equals(DroneStateEnum.DELIVERING)) {
			throw new DroneDeliveryException("Drone is not in DELIVERING mode.");
		}
		
		drone.setState(DroneStateEnum.RETURNING);
		this.droneRepository.save(drone);
		
		order.setStatus(DeliveryOrderStatusEnum.DELIVERED);
		order.setCompletedOn(new Date());
		order.setIsActive(0);
		this.deliveryOrderRepository.save(order);
		return drone;
	}

	@Override
	@Transactional
	public Drone setOrderDeliveryFailed(int orderId) {
		DeliveryOrder order = this.getActiveOrder(orderId);
		
		if (!order.getStatus().equals(DeliveryOrderStatusEnum.DEPARTED)) {
			throw new DroneDeliveryException("Delivery Order is not in DEPARTED mode.");
		}
		
		Drone drone = order.getDrone();
		if (!drone.getState().equals(DroneStateEnum.DELIVERING)) {
			throw new DroneDeliveryException("Drone is not in DELIVERING mode.");
		}
		
		drone.setState(DroneStateEnum.RETURNING);
		this.droneRepository.save(drone);
		
		order.setStatus(DeliveryOrderStatusEnum.DELIVERY_FAILED);
		this.deliveryOrderRepository.save(order);
		return drone;
	}

	@Override
	@Transactional
	public Drone droneReturnedToFleet(String droneSerialNumber) {
		Optional<Drone> droneOptional = this.droneRepository.findById(droneSerialNumber);
		if (!droneOptional.isPresent()) {
			throw new ObjectNotFoundException("Drone not found for serial number: " + droneSerialNumber);
		}
		Drone drone = droneOptional.get();
		if (!drone.getState().equals(DroneStateEnum.RETURNING)) {
			throw new DroneDeliveryException("Drone is not in RETURNING mode.");
		}
		drone.setState(DroneStateEnum.IDLE);
		this.droneRepository.save(drone);
		
		DeliveryOrder order = this.deliveryOrderRepository.findDeliveryOrderByStatusAndDrone(droneSerialNumber);
		if (order != null) {
			order.setCompletedOn(new Date());
			order.setIsActive(0);
			this.deliveryOrderRepository.save(order);
		}
		return drone;
	}

	@Override
	@Transactional
	public Drone updateDroneBatteryCapacity(Drone drone, int batteryCapacity) {
		DroneBatteryStatusHistory history = new DroneBatteryStatusHistory();
		history.setCheckedOn(new Date());
		history.setDrone(drone);
		history.setNewReading(batteryCapacity);
		history.setPreviousReading(drone.getBatteryCapacity());
		this.droneBatteryStatusHistoryRepository.save(history);
		
		drone.setBatteryCapacity(batteryCapacity);
		this.droneRepository.save(drone);
		
		return drone;
	}
	
}

package com.rumaiz.dronedelivery.service;

import java.util.List;

import com.rumaiz.dronedelivery.model.Medication;

public interface MedicationService {
	public void insert(Medication medication);
	public List<Medication> getAll();
	public Medication getByCode(String code);
}

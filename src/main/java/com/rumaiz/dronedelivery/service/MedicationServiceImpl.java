package com.rumaiz.dronedelivery.service;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rumaiz.dronedelivery.exceptions.InvalidInputDataException;
import com.rumaiz.dronedelivery.exceptions.ObjectNotFoundException;
import com.rumaiz.dronedelivery.model.Medication;
import com.rumaiz.dronedelivery.repository.MedicationRepository;

@Service
public class MedicationServiceImpl implements MedicationService{
	
	private MedicationRepository medicationRepository;
	
	@Autowired
	public void setMedicationRepository(MedicationRepository medicationRepository) {
		this.medicationRepository = medicationRepository;
	}

	@Override
	public void insert(Medication medication) {
		if (medication.getName().length() > 100) {
			throw new InvalidInputDataException("Name should be maximum 100 characters");
		}
		
		if (!Pattern.matches("^[a-zA-Z0-9_-]*$", medication.getName())) {
			throw new InvalidInputDataException("Invalid name. Only letters, numbers, hyphen and underscore is allowed.");
		}
		
		if (!Pattern.matches("^[A-Z0-9_]*$", medication.getCode())) {
			throw new InvalidInputDataException("Invalid Code. Only uppercase letters, numbers and underscore is allowed.");
		}
		
		this.medicationRepository.save(medication);		
	}

	@Override
	public List<Medication> getAll() {
		return (List<Medication>) this.medicationRepository.findAll();
	}

	@Override
	public Medication getByCode(String code) {
		Optional<Medication> medicationOptional = this.medicationRepository.findById(code);
		if (medicationOptional.isEmpty()) {
			throw new ObjectNotFoundException("Medication not found for code: " + code);
		}
		return medicationOptional.get();
	}
	
}

/**Seed Drone data*/
INSERT INTO drone (serial_number, model, weight_limit, battery_capacity, state) VALUES ('0000001111', 'MIDDLE_WEIGHT', 300, 80, 'IDLE');
INSERT INTO drone (serial_number, model, weight_limit, battery_capacity, state) VALUES ('0000001112', 'LIGHT_WEIGHT', 100, 20, 'IDLE');
INSERT INTO drone (serial_number, model, weight_limit, battery_capacity, state) VALUES ('0000001113', 'MIDDLE_WEIGHT', 350, 75, 'IDLE');
INSERT INTO drone (serial_number, model, weight_limit, battery_capacity, state) VALUES ('0000001114', 'MIDDLE_WEIGHT', 325, 50, 'IDLE');
INSERT INTO drone (serial_number, model, weight_limit, battery_capacity, state) VALUES ('0000001115', 'LIGHT_WEIGHT', 150, 10, 'IDLE');
INSERT INTO drone (serial_number, model, weight_limit, battery_capacity, state) VALUES ('0000001116', 'MIDDLE_WEIGHT', 250, 90, 'IDLE');
INSERT INTO drone (serial_number, model, weight_limit, battery_capacity, state) VALUES ('0000001117', 'HEAVY_WEIGHT', 500, 100, 'IDLE');
INSERT INTO drone (serial_number, model, weight_limit, battery_capacity, state) VALUES ('0000001118', 'LIGHT_WEIGHT', 125, 60, 'IDLE');
INSERT INTO drone (serial_number, model, weight_limit, battery_capacity, state) VALUES ('0000001119', 'CRUISER_WEIGHT', 400, 80, 'IDLE');
INSERT INTO drone (serial_number, model, weight_limit, battery_capacity, state) VALUES ('00000011110', 'HEAVY_WEIGHT', 500, 40, 'IDLE');

/**Seed Medication Data*/
INSERT INTO medication (code, name, weight, image_path) VALUES ('ASK123456', 'Synthroid-levothyroxine', 50, '/images/ASK123456.jpg');
INSERT INTO medication (code, name, weight, image_path) VALUES ('DWU123_239086', 'Crestor-rosuvastatin', 100, '/images/DWU123-239086.jpg');
INSERT INTO medication (code, name, weight, image_path) VALUES ('FDK_34_19074565', 'Ventolin-HFA-albuterol', 400, '/images/800wm.jpg');
INSERT INTO medication (code, name, weight, image_path) VALUES ('DFG12_78964', 'Nexium_esomeprazole', 150, '/images/DFG12-78964.jpg');
INSERT INTO medication (code, name, weight, image_path) VALUES ('RTR_12K3456', 'Advair_Diskus-fluticasone', 450, '/images/RTR-12K3456.jpg');
